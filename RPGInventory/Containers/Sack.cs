﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPGInventory.items;

namespace RPGInventory.Containers
{
    class Sack : Bag
    {
        public Sack(int capacity) : base(capacity)
        {

        }

        public override void AddItem(Item itemToAdd)
        {
            if(itemToAdd.Weight > 1)
            {
                Console.WriteLine("That item is to heavy for the sack!");
            }
            else
            {
                base.AddItem(itemToAdd);
            }
            
        }
    }
}
