﻿using RPGInventory.Containers;
using RPGInventory.items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGInventory
{
    class Program
    {
        static void Main(string[] args)
        {
            //BagTest();
            //SackTest();
            RemoveTest();
            Console.ReadLine();

        }

        private static void RemoveTest()
        {
            Bag myBag = new Bag(3);

            var item1 = new Sword();
            var item2 = new Potion();
            var item3 = new Sword();

            myBag.AddItem(item1);
            myBag.AddItem(item2);
            myBag.AddItem(item3);

            myBag.DisplayContent();

            var removed = myBag.RemoveItem();
            Console.WriteLine("Removed item: {0}", removed.Name);

            myBag.DisplayContent();

        }

        private static void BagTest()
        {
            Bag myBag = new Bag(3);

            var item1 = new Sword();
            var item2 = new Potion();

            myBag.AddItem(item1);
            myBag.AddItem(item2);

            myBag.DisplayContent();

        }

        private static void SackTest()
        {
            Sack mySack = new Sack(4);

            var item1 = new Sword();
            var item2 = new Potion();

            mySack.AddItem(item2);
            mySack.AddItem(item1);

            mySack.DisplayContent();
        }
    }
}
