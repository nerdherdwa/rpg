﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGInventory.items
{
    class Sword : Item
    {
        public Sword()
        {
            Name = "a wooden sword";
            Weight = 3;
        }
    }
}
